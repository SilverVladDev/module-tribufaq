<?php

if (!defined('_PS_VERSION_')) {
    exit;
}
class TribufaqCategory extends ObjectModel
{
    public $id_tribufaq_category;
    public $name;
    public $active;
    public $date_add;

    protected $questions;


    public static $definition = [
        'table'     => 'tribufaq_category',
        'primary'   => 'id_tribufaq_category',
        'multilang' => true,
        'fields'    => [
            'name'     => ['type' => self::TYPE_STRING, 'required' => true, 'lang' => true, 'validate' => 'isGenericName'],
            'active'   => ['type' => self::TYPE_BOOL, 'validate' => 'isBool', 'active' => 'status'],
            'date_add' => ['type' => self::TYPE_DATE, 'required' => false]
        ],
    ];

    public function getCategoryName($id_tribufaq_category)
    {
        $query = new DbQuery();
        $query->from('tribufaq_category', 'fc');
        $query->select('name');
        $query->leftJoin('tribufaq_category_lang', 'fcl', 'fc.id_tribufaq_category = fcl.id_tribufaq_category AND id_lang=' . Context::getContext()->language->id);
        $query->where('fc.id_tribufaq_category = '.$id_tribufaq_category);
        return Db::getInstance()->getValue($query);
    }

    public function getActiveCategoriesForSelect()
    {
        $query = new DbQuery();
        $query->from('tribufaq_category', 'fc');
        $query->select('fc.id_tribufaq_category as id_category, name');
        $query->leftJoin('tribufaq_category_lang', 'fcl', 'fc.id_tribufaq_category = fcl.id_tribufaq_category');
        $query->where('fc.active = 1 AND fcl.id_lang=' . Context::getContext()->language->id);

        return Db::getInstance()->executeS($query);
    }

    public function getQuestions()
    {
        if (!$this->questions) {
            $tribufaqQuestion = new TribufaqQuestion();            
            $this->questions = $tribufaqQuestion->getQuestionsByCategoryId($this->id_tribufaq_category);
        }
        return $this->questions;
    }

    public static function getCategoriesWithQuestions($limit = 5)
    {
        $sql = new DbQuery();
        $sql->select('c.*, ql.question, ql.response');
        $sql->from('tribufaq_category', 'c');
        $sql->innerJoin('tribufaq_question', 'q', 'q.id_tribufaq_category = c.id_tribufaq_category');
        $sql->innerJoin('tribufaq_question_lang', 'ql', 'q.id_tribufaq_question = ql.id_tribufaq_question AND ql.id_lang = ' . (int) Context::getContext()->language->id);
        $sql->orderBy('q.date_add DESC');
        $sql->groupBy('c.id_tribufaq_category');
        $sql->limit($limit);

        $results = Db::getInstance()->executeS($sql);

        $categories = array();
        if ($results) {
            foreach ($results as $result) {
                $category = new TribuFaqCategory((int) $result['id_tribufaq_category']);
                $category->name = $result['name'];
                $category->questions[] = array(
                    'question' => $result['question'],
                    'response' => $result['response'],
                );
                $categories[] = $category;
            }
        }

        return $categories;
    }
}