{extends file='admin/layout.tpl'}

{block name="content"}
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-cogs"></i> {$module->l('Configuration de Tribu FAQ')}
        </div>
        <div class="panel-body">
            <!-- Votre formulaire de configuration ici -->
            <form action="{$smarty.server.PHP_SELF|escape:'html':'UTF-8'}" method="post" class="defaultForm form-horizontal">
                <!-- Champs de formulaire -->
                <div class="form-group">
                    <label class="control-label col-lg-3">Paramètre 1 :</label>
                    <div class="col-lg-9">
                        <input type="text" name="param1" class="form-control" />
                    </div>
                </div>
                <!-- Autres champs de formulaire -->
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <button type="submit" name="submitTribufaqConfig" class="btn btn-default pull-right">
                            <i class="process-icon-save"></i> {$module->l('Enregistrer')}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
{/block}
