<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class AdminTribufaqQuestionController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->bulk_actions = array();
        $this->context = Context::getContext();
        $this->id_lang = $this->context->language->id;
        $this->shop = $this->context->shop->id;
        $this->table = 'tribufaq_question'; // nom de la table
        $this->identifier = 'id_tribufaq_question'; // primary key de la table
        $this->default_form_language = $this->context->language->id;
        $this->bootstrap = true;
        $this->controller_name = 'AdminTribufaqQuestionController';
        $this->className = 'TribufaqQuestion'; // nom de la classe de l'objet
        $this->lang = true;

        parent::__construct();

        // liste des champs à afficher dans la liste des Questions/Réponses
        $this->fields_list = [
            'id_tribufaq_question' => [ // Nom du champ SQL
                'title' => 'ID', // Nom affiché dans le tableau
                'align' => 'center', // Alignement
                'class' => 'fixed-width-xs', // Classe CSS de l'élément
            ],
            'question' => [
                'title' => $this->module->l('Question'),
                'align' => 'left',
                'lang' => true,
            ],
            'response' => [
                'title' => $this->module->l('Réponse'),
                'align' => 'left',
                'lang' => true,
            ],
            'id_tribufaq_category' => [
                'title' => $this->module->l('Category'),
                'align' => 'left',
                'callback' => 'formatCategory',
            ],
            'date_add' => [
                'title' => $this->module->l('Date de création'),
                'align' => 'center',
            ],
            'active' => [
                'title' => $this->module->l('Active'),
                'align' => 'center',
                'type' => 'bool',
                'active' => 'toggleActive',
                'ajax' => true,
            ],
        ];

        // actions disponibles pour chaque ligne
        $this->addRowAction('edit');
        $this->addRowAction('delete');
    }

    public function initContent()
    {
        parent::initContent();
    }

    /**
     * Gestion de la toolbar
     */
    public function initPageHeaderToolbar()
    {
        // Bouton d'ajout
        $this->page_header_toolbar_btn['new'] = array(
            'href' => self::$currentIndex . '&add' . $this->table . '&token=' . $this->token,
            'desc' => $this->module->l('Ajouter une FAQ'),
            'icon' => 'process-icon-new',
        );

        parent::initPageHeaderToolbar();
    }

    /**
     * Gestion du formulaire de création/édition
     */
    public function renderForm()
    {
        $this->loadObject(true);
        // définition du formulaire et champs

        $this->fields_form = [
            'legend' => [
                'title' => $this->module->l('FAQ'),
                'icon' => 'icon-cog',
            ],
            'input' => [
                [
                    'type' => 'text',
                    'label' => $this->module->l('Question'),
                    'name' => 'question',
                    'lang' => true,
                    'required' => true,
                ],
                [
                    'type' => 'textarea',
                    'label' => $this->module->l('Réponse'),
                    'name' => 'response',
                    'lang' => true,
                    'required' => true,
                    'autoload_rte' => true,
                ],
                [
                    'type' => 'select',
                    'label' => $this->module->l('Catégorie'),
                    'name' => 'id_tribufaq_category',
                    'required' => true,
                    'options' => [
                        'query' => (new TribufaqCategory())->getActiveCategoriesForSelect(), // Appel correct de la méthode non statique
                        'id' => 'id_category',
                        'name' => 'name',
                    ],
                ],
                [
                    'type' => 'switch',
                    'label' => $this->context->getTranslator()->trans('Active', [], 'Admin.Global'),
                    'name' => 'active',
                    'required' => false,
                    'is_bool' => true,
                    'values' => [
                        [
                            'id' => 'active_on',
                            'value' => true,
                            'label' => $this->context->getTranslator()->trans('Yes', [], 'Admin.Global'),
                        ],
                        [
                            'id' => 'active_off',
                            'value' => false,
                            'label' => $this->context->getTranslator()->trans('No', [], 'Admin.Global'),
                        ],
                    ],
                ],
            ],
            'submit' => [
                'title' => $this->context->getTranslator()->trans('Save', [], 'Admin.Actions'),
            ],
        ];

        return parent::renderForm();
    }

    public function ajaxProcessToggleActiveTribufaqQuestion()
    {
        $tribufaq = new TribufaqQuestion(Tools::getValue('id_tribufaq_question'));
        $tribufaq->active = !$tribufaq->active;

        if ($tribufaq->save()) {
            die(Tools::jsonEncode([
                'success' => 1,
                'text' => $this->trans('The settings have been successfully updated.', [], 'Admin.Notifications.Success'),
            ]));
        } else {
            die(Tools::jsonEncode([
                'success' => 0,
                'text' => $this->trans('Unable to update settings.', [], 'Admin.Notifications.Error'),
            ]));
        }
    }

    public function formatCategory($value, $row)
    {
        // Utiliser getCategoryName pour obtenir le nom de la catégorie
        $category_name = (new TribufaqCategory())->getCategoryName($value);

        return $category_name;
    }
}
