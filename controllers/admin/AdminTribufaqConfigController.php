<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once _PS_MODULE_DIR_ . '/tribufaq/classes/ModuleClassUtility.php';

class AdminTribufaqConfigController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'module_config'; // Tableau fictif pour les configurations
        $this->className = 'TribuFaqConfig'; // Classe de configuration fictive
        $this->lang = false;
        $this->explicitSelect = true;
        $this->addRowAction('edit');
        $this->addRowAction('delete');

        parent::__construct();
    }
}
