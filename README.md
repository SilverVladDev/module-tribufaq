# Test Module TribuFaq

Celui-ci est composé de 2 parties :

• une partie QCM : (Temps estimatif 20 min)
Rendez-vous sur : 
https://docs.google.com/forms/d/e/1FAIpQLSdIR-Iz4OgkYMZzppqWaKnUNbsh99qrsOcyshzeHibJZzEa6g/viewform?usp=sf_link
Un conseil : soit on sait... soit on ne sait pas... ne passez pas trop de temps dessus.

• un module Prestashop à modifier
Vous trouverez ci-après le lien qui vous permettra de télécharger une archive zip contenant un brief et le module de base : https://drive.google.com/file/d/1cqP0QNHSe1QNV7Ti5iO3xzJcgHqlCc8O/view?usp=sharing

Un conseil : L'objectif est d'en faire le plus possible en 3H. Imaginez-vous que votre client vous met la pression et qu'il faut sortir quelque chose coûte que coûte !! Nous avons bien conscience qu'il sera difficile de finir. N'hésitez pas à procéder par itération et exploitez les éléments à votre disposition.

Merci de nous renvoyer le module sur le repo GIT de votre choix.